import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class SelectParent implements Runnable {
    private int size;
    private int populationSize;
    private ArrayList<Individual> individuals;
    private Random random = new Random();
    private BlockingQueue<ArrayList<Individual>> tempParentIndividualQueue;
    
    public SelectParent(BlockingQueue<ArrayList<Individual>> tempParentIndividualQueue, ArrayList<Individual> individuals, int size, int populationSize){
        this.individuals = individuals;
        this.size = size;
        this.populationSize = populationSize;
        this.tempParentIndividualQueue = tempParentIndividualQueue;
    }

    @Override
    public void run() {
        ArrayList<Individual> tempParentArray = new ArrayList<>();
        tempParentArray.add(selectingParents());
        tempParentArray.add(selectingParents());

        try {
            tempParentIndividualQueue.put(tempParentArray);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public Individual selectingParents() {
        int chromosome_index = random.nextInt(populationSize);
        Individual parent = individuals.get(chromosome_index);

        for (int i = 0; i < size; i++) {
            if (individuals.get(chromosome_index).individualEvaluation() <= parent.individualEvaluation()){
                parent = individuals.get(chromosome_index);
            }
            chromosome_index = random.nextInt(populationSize);
        }

        return parent; 
    }
}
